import tkinter as tk 
from tkinter import ttk

win = tk.Tk()

labelfram = tk.LabelFrame(win, text="Python GUI Programming coockBook")
labelfram.grid(column=0, row=0)

# label 
labelName = tk.Label(labelfram, text="Enter a Name")
labelName.grid(column=1, row=1)

labelName2 = tk.Label(labelfram, text="Choose a Number")
labelName2.grid(column=2, row=1, spanx=4)

# entry 
entryName = tk.Entry(labelfram, text="<name>")
entryName.grid(column=1, row=2,spanx=4)

# combo 
varCombo = tk.StringVar()
comboName = ttk.Combobox(labelfram, text="<name>")
comboName.grid(column=2, row=2)
comboName['values'] = (1,2,3,4)
comboName.current(0)

# button 
enButton = tk.Button(labelfram, text="Click me")
enButton.grid(column=3, row=2)

win.mainloop()
