####################
#   IMPORTS
####################
import tkinter as tk
from tkinter import ttk, Menu, LabelFrame
from tkinter import scrolledtext
from tkinter import messagebox as mBox
from threading import Thread
import time
from queue import Queue

class Poo():
    def __init__(self):
        self.win = tk.Tk()
        self.win.title("Python GUI")
        self.win.iconbitmap(r'E:/tuto/GUI/logo.png')
        self.createMenu()
        self.createTable()
        self.createWidgets()
        self.createfiles()

    def _msgBox(self):
        mBox.showinfo('Python Message Info Box', 
        'A Python GUI created using tkinter:\nThe year is 2015.')
        mBox.showwarning('Python Message Warning Box', 
        'A Python GUI created using tkinter:\nWarning: There might be a bug in this code.')
        self.answer = mBox.askyesno(" question", "are you sure i can fuck you?")
        print(self.answer)

    def clickMe(self):
        self.action.configure(text="Hello " + self.name.get())
        self.createThread()
        print(self)

    
    def _spin(self):
        value = self.spin.get()
        print(value)
        self.scr.insert(tk.INSERT, value + "\n")
    
    def methodThread(self,numOfLoops=10):
        for idx in range(numOfLoops):
            time.sleep(1)
            self.scr.insert(tk.INSERT, str(idx) + '\n')
        time.sleep(1)
        print('methodInAThread():', self.runT.isAlive())


    
    def createThread(self):
        self.runT = Thread(target=self.methodThread, args=[8])
        self.runT.setDaemon(True)
        self.runT.start()
        print(self.runT)
        print('createThread():', self.runT.isAlive())

    def useQueues(self):
        guiqueueu = Queue()
        print(guiqueueu)
        for idx in range(10):
            guiqueueu.put('Message from a queue')
        while True:
            print(guiqueueu.get())

    
    def createMenu(self):
        self.menuBar = Menu(self.win)
        self.win.config(menu=self.menuBar)

        self.helpMenu = Menu(self.menuBar, tearoff=0)
        self.helpMenu.add_command(label="About", command=self._msgBox)
        self.menuBar.add_cascade(menu=self.helpMenu, label="Help")

    def createTable(self):
        self.tabControl = ttk.Notebook()
        self.tab1 = ttk.Frame(self.tabControl)
        self.tab2 = ttk.Frame(self.tabControl)
        self.tabControl.add(self.tab1, text=" Tab 1")
        self.tabControl.add(self.tab2, text=" Tab 2")
        self.tabControl.pack(expand=1, fill='both')

    def createWidgets(self):
        # labelframe
        self.frameOne = tk.LabelFrame(self.tab1, text="Mounty Python")
        self.frameOne.grid(column=0, row=0, padx=10, pady=10)
        # label
        self.alabel = ttk.Label(self.frameOne, text="Enter a name: ").grid(column=0,row=0,sticky='W')
        self.blabel = ttk.Label(self.frameOne, text="Choose an Number: ").grid(column=0,row=1)
        # entry
        self.name = tk.StringVar()
        self.nameEnter = ttk.Entry(self.frameOne, width=24, textvariable=self.name).grid(column=0, row=1,sticky='W')
        # combobox 
        self.number = tk.IntVar()
        self.nbShoosen = ttk.Combobox(self.frameOne,width=14, textvariable=self.number)
        self.nbShoosen['values'] = (1, 2, 4, 42, 100)
        self.nbShoosen.grid(column=1, row=1)
        self.nbShoosen.current(0)
        # button 
        self.action = ttk.Button(self.frameOne, text="Click Me", command=self.clickMe)
        self.action.grid(column=2, row=1,sticky='W')        # spin 
        # spinbox
        self.spin = tk.Spinbox(self.frameOne, values=(1, 2, 4, 42, 100), width=5,bd=8, command=self._spin)
        self.spin.grid(column=0, row=2, sticky='W')
        # scrolledtext 
        self.scrolW = 40; self.scrolH = 10
        self.scr = scrolledtext.ScrolledText(self.frameOne,width=self.scrolW, height=self.scrolH, wrap=tk.WORD )
        self.scr.grid(column=0,row=3,sticky='WE',columnspan=3)

    def getFileName():
        print('hello from getFileName')

    def createfiles(self):
        self.mngFilesFrame = ttk.LabelFrame(self.tab2, text=' Manage Files: ')
        self.mngFilesFrame.grid(column=0, row=1, sticky='WE', padx=10, pady=5)
        self.lb = ttk.Button(self.mngFilesFrame, text="Browse to File...", command=self.getFileName)
        self.lb.grid(column=0, row=0, sticky=tk.W)
        tfile = tk.StringVar()
        self.entryLen = self.scrolW
        self.fileEntry = ttk.Entry(self.mngFilesFrame, width=self.entryLen,textvariable=tfile)
        self.fileEntry.grid(column=1, row=0, sticky=tk.W)

        logDir = tk.StringVar()
        self.netwEntry = ttk.Entry(self.mngFilesFrame, width=self.entryLen,textvariable=logDir)
        self.netwEntry.grid(column=1, row=1, sticky=tk.W)

    # def copyFile():
    #     import shutil
    #     src = self.fileEntry.get()
    #     file = src.split('/')[-1]
    #     dst = self.netwEntry.get() + '\\'+ file
    #     try:
    #         shutil.copy(src, dst)
    #         mBox.showinfo('Copy File to Network','Success: File copied.')









op = Poo()
op.win.mainloop()