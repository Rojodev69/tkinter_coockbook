import tkinter as teka
from tkinter import ttk, Menu, messagebox, Spinbox, scrolledtext


win = teka.Tk()
win.title("RJ-TEKA")

def _msgbox():
    messagebox.showinfo("info","CECI EST UN INFO")

menubar = Menu(win)
win.config(menu=menubar)
helpMenu = Menu(menubar, tearoff=0)
helpMenu.add_command(label="Help", command=_msgbox)
menubar.add_cascade(menu=helpMenu, label="Help")

tabControl = ttk.Notebook(win)

tab1 = ttk.Frame(tabControl)
tabControl.add(tab1, text="Tab 1")
tab2 = ttk.Frame(tabControl)
tabControl.add(tab2, text="Tab 2")

tab3 = ttk.Frame(tabControl)
tabControl.add(tab3, text="Tab 3")
tabControl.pack(fill="both")

framOne = ttk.LabelFrame(tab1, text="labelFrame 1")
framOne.grid(column=0, row=0,padx=4, pady=4)

label = ttk.Label(framOne, text="Label 1").grid(column=1, row=0, sticky='W')

def _spin():
    value = spinOne.get()
    scr.insert(tk.INSERT,value + '\n')

labe2 = ttk.Label(tab2, text="Label 2").grid(column=0, row=0)

scr = scrolledtext.ScrolledText(tab2,width=30, height=5).grid(column=0,row=3, columnspan=3, pady=5)

spinOne = Spinbox(tab2, from_=0, to=10,width=10, bd=2, command=_spin)
spinOne.grid(column=0, row=1)


win.mainloop()
