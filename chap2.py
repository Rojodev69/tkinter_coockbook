#importation
import tkinter as tk
from tkinter import ttk, Menu
from tkinter import scrolledtext

#fenetre
win = tk.Tk()
win.title("ROJO")
monty = ttk.LabelFrame(win, text=' Monty Python ')
monty.grid(column=0, row=0)
# win.resizable(0,0)






monty = ttk.LabelFrame(tab1, text=' Monty Python ')
monty.grid(column=0, row=0, padx=8, pady=4)
ttk.Label(monty, text="Enter a name:").grid(column=0, row=0,
sticky='W')





#global variable
colors = ["Blue", "Gold", "Red"]
#Fonction
def clickMe():
    action.configure(text="Hello " + name.get()+ ' '+number.get())

def radCall():
    radSel = radVar.get()
    if radSel == 0:  monty.configure(background=colors[0])
    elif radSel == 1:  monty.configure(background=colors[1])
    elif radSel == 2:  monty.configure(background=colors[2])
def _quit():
    win.quit()
    win.destroy()
    exit() 

#Menu
menuBar = Menu(win)
win.config(menu=menuBar)
fileMenu = Menu(menuBar,tearoff=0)
fileMenu.add_command(label="New")
fileMenu.add_separator()
fileMenu.add_command(label="Exit", command=_quit)
menuBar.add_cascade(label="File", menu=fileMenu)
helpmenu = Menu(menuBar, tearoff=0)
helpmenu.add_command(label="About")
menuBar.add_cascade(label="Help", menu=helpmenu)


#label
alabel = ttk.Label(monty, text="Enter the name")
alabel.grid(column=0, row=0) 
blabel = ttk.Label(monty, text="Enter the number")
blabel.grid(column=1, row=0) 

#entry
name = tk.StringVar()
nameEnter = ttk.Entry(monty, width=12, textvariable=name)
nameEnter.grid(column=0, row=1)

#button
action = ttk.Button(monty, text="Click Me", command=clickMe)
action.grid(column=2, row=1)

#combobox
number = tk.StringVar()
nbChosen = ttk.Combobox(monty, width=12, textvariable=number)
nbChosen['values'] = (1,2,4,6)
nbChosen.grid(column=1, row=1)
nbChosen.current(0)

#checkbox
chVarDis = tk.IntVar()
check1 = tk.Checkbutton(monty, text="Disabled",variable=chVarDis, state="disabled")
check1.select() 
check1.grid(column=0, row=2)
chVarUnchek = tk.IntVar()
check2 = tk.Checkbutton(monty, text="UnChecked",variable=chVarUnchek)
check2.deselect()
check2.grid(column=1, row=2)
chVarChek = tk.IntVar()
check3 = tk.Checkbutton(monty, text="Enabled",variable=chVarChek)
check3.select() 
check3.grid(column=2, row=2)

# radio
radVar = tk.IntVar()
radVar.set(99) 

for col in range(3):
    curRad = 'rad' + str(col)
    curRad = tk.Radiobutton(monty, text=colors[col], variable=radVar, value=col, command=radCall)
    curRad.grid(column=col, row=5)

#scrolledtext
scr = scrolledtext.ScrolledText(monty, width=30, height=5, wrap=tk.WORD)
scr.grid(column=0, columnspan=3)

# labelFrame

labelsFrame = ttk.LabelFrame(monty, text="Frame for label")
labelsFrame.grid(column=0, row=7)

ttk.Label(labelsFrame, text="Label1").grid(column=0, row=0)
ttk.Label(labelsFrame, text="Label2").grid(column=1, row=0)
ttk.Label(labelsFrame, text="Label3").grid(column=2, row=0)

# for child in labelsFrame.winfo_children():
#     child.grid_configure(padx=8, pady=4)


for child in monty.winfo_children():
    child.grid_configure(sticky='WE')





win.mainloop()