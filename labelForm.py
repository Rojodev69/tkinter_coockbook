import tkinter as tk 
from tkinter import ttk, scrolledtext

win = tk.Tk()
win.resizable(0,0)

frameOne = tk.LabelFrame(win, text="Python GUI Programming coockbook")
frameOne.grid(column=0, row=0, padx=10, pady=10)

varChek = tk.IntVar()
chekbox1 = tk.Checkbutton(frameOne, text="check", variable=varChek)
chekbox1.grid(column=1, row=0)
chekbox1.select()


frameTwo = tk.LabelFrame(win, text="Type into the scrolled text control")
frameTwo.grid(column=0, row=2, padx=10, pady=50)

scrolledText1 = scrolledtext.ScrolledText(frameTwo,width=30, height=5)
scrolledText1.grid(column=0, columnspan=3)

win.mainloop()