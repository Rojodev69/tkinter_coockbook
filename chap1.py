#importation
import tkinter as tk
from tkinter import ttk
from tkinter import scrolledtext

#fenetre
win = tk.Tk()
win.title("ROJO")
# win.resizable(0,0)

#global variable
colors = ["Blue", "Gold", "Red"]
#Fonction
def clickMe():
    action.configure(text="Hello " + name.get()+ ' '+number.get())

def radCall():
    radSel = radVar.get()
    if radSel == 0:  win.configure(background=colors[0])
    elif radSel == 1:  win.configure(background=colors[1])
    elif radSel == 2:  win.configure(background=colors[2])

#label
alabel = ttk.Label(win, text="Enter the name")
alabel.grid(column=0, row=0) 
blabel = ttk.Label(win, text="Enter the number")
blabel.grid(column=1, row=0) 

#entry
name = tk.StringVar()
nameEnter = ttk.Entry(win, width=12, textvariable=name)
nameEnter.grid(column=0, row=1, sticky=tk.W)

#button
action = ttk.Button(win, text="Click Me", command=clickMe)
action.grid(column=2, row=1, sticky=tk.W)

#combobox
number = tk.StringVar()
nbChosen = ttk.Combobox(win, width=12, textvariable=number)
nbChosen['values'] = (1,2,4,6)
nbChosen.grid(column=1, row=1, sticky=tk.W)
nbChosen.current(0)

#checkbox
chVarDis = tk.IntVar()
check1 = tk.Checkbutton(win, text="Disabled",variable=chVarDis, state="disabled")
check1.select() 
check1.grid(column=0, row=2,sticky=tk.W)
chVarUnchek = tk.IntVar()
check2 = tk.Checkbutton(win, text="UnChecked",variable=chVarUnchek)
check2.deselect()
check2.grid(column=1, row=2,sticky=tk.W)
chVarChek = tk.IntVar()
check3 = tk.Checkbutton(win, text="Enabled",variable=chVarChek)
check3.select() 
check3.grid(column=2, row=2,sticky=tk.W)

# radio
radVar = tk.IntVar()
radVar.set(99) 
# rad1 = tk.Radiobutton(win, text="Blue", variable=radVar, value=1, command=radCall)
# rad1.grid(column=0, row=3,sticky=tk.W)
# rad2 = tk.Radiobutton(win, text="Green", variable=radVar, value=2, command=radCall)
# rad2.grid(column=1, row=3,sticky=tk.W)
# rad3 = tk.Radiobutton(win, text="Red", variable=radVar, value=3, command=radCall)
# rad3.grid(column=2, row=3,sticky=tk.W)
for col in range(3):
    curRad = 'rad' + str(col)
    curRad = tk.Radiobutton(win, text=colors[col], variable=radVar, value=col, command=radCall)
    curRad.grid(column=col, row=5)

#scrolledtext
scr = scrolledtext.ScrolledText(win, width=30, height=5, wrap=tk.WORD)
scr.grid(column=0, columnspan=3)



win.mainloop()